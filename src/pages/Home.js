import Banner from '../components/Banner';
import Highlights from '../components/Highlights';
// import CourseCard from '../components/CourseCard';


export default function Home(){
	
 return (
    <>
      <Banner title="B248 Booking App" 
      description="Opportunities for everyone, everywhere!" 
      buttonText="Enroll Now!"
      buttonVariant ="primary"
      />      
      <Highlights/>
      {/*<CourseCard/>*/}
    </>
  )
}

